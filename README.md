# JavaScript Examples Collection
An collection of sample JS files, crafted with love by the Pins team, currently maintainned by Andrei Jiroh. PRs welcome!

## Testing examples
1. Clone this repository to your machine.
2. Navigate to the local repository directory.
3. Run `./collections-api-js setup-nodejs` to setup Node.js for your machine. This will install [Node Version Manager](https://github.com/nvm-sh/nvm)
on your macOS/Linux machine. For Windows users, download from the Node.js website.
4. Navigate to the `examples` directory and do `ls -la` to see what categories are available then select with `cd <category-goes-here>`.
5. Once you're there, install dependencies with `npm install`.
6. Read the accompied README bfore yeeting.

## Contributing
See the `CONTRIBUTING.md` for details on how to contribute to the project.

## License
MIT License
